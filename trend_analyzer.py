from web_crawler import *
from excel_handler import *


print("Progress: Crawling a website")
title_list, hit_count_list = web_crawling()

print("Progress: Creating an excel report")
excel_handling(title_list, hit_count_list)

