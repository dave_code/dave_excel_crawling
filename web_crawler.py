from urllib.request import urlopen
from bs4 import BeautifulSoup
import re


def web_crawling():
    hit_list = []
    title_list = []

    for pageNum in range (10):
        html = urlopen ("http://www.clien.net/cs2/bbs/board.php?bo_table=news&page=" + str (pageNum + 1))
        bsObj = BeautifulSoup (html, "html.parser")
        tdListView = bsObj.findAll ("td")
        hit_list_count, title_list_count = 0, 0

        for num, tdData in enumerate (tdListView):
            if re.match ('<td>[0-9]*</td>', str (tdData)):
                if re.match('<td>[0-9]{7}</td>', str(tdData)):
                    continue
                else:
                    hit_list_count += 1
                    if hit_list_count > 2:
                        hit_list.append(str(tdData)[4:].replace('</td>', ''))

        tdPostList = bsObj.findAll ("td", {"class": "post_subject"})
        for tdPost in tdPostList:
            if tdPost.find ("a") is not None:
                title_list_count += 1
                if title_list_count > 2:
                    title_list.append (tdPost.find ("a").get_text ().replace ('\'', ''))

    return title_list, hit_list
